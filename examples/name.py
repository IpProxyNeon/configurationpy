import configurationpy
try: # Try to do the following: 
    db = Confpy.access("nametest") # This will throw an error if doesn't exist, and will continue to reexceptscue
    name = db.readcfg('name.cfg')
    print(f"Hey, {name}! I got your name from the configuration file!")
except FileNotFoundError: # If the above fails, does the following: 
    db = Confpy.new('nametest')
    name = input("What's your name?")
    db.mkcfg('name.cfg', name)
    print(f"Hey, {name}! I got your name from user input!")